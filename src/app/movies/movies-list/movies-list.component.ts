import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { MoviesListService } from './services/movies-list.service';
import { Movies } from './models/movies-list-model';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit {

  public postersMoviesList: Movies[]; // Listado de péliculas.


  constructor(private moviesListService: MoviesListService, private router: Router) {/* EMPTY */ }

  ngOnInit(): void {
    this.moviesListService.getPostersFilmList().subscribe((data: Movies[]) => {

      this.postersMoviesList = data;


    }, (err: any) => {
      console.error(err);
    });
  }
  public navigateTo(id: number): void {
    /* this.router.navigate([index]); */   /* esto es angular 9 y funciona */
    this.router.navigateByUrl('movies/' + id);  /* angular 10 y tambien funciona */
  }/* especificas la url por la que se va a navegar */
}
