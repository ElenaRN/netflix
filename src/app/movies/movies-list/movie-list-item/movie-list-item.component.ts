import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Movies } from '../models/movies-list-model';

@Component({
  selector: 'app-movie-list-item',
  templateUrl: './movie-list-item.component.html',
  styleUrls: ['./movie-list-item.component.scss']
})
export class MovieListItemComponent implements OnInit {

  @Input() posterMovie: Movies;
  @Input() index: number;
  @Output() navigate: EventEmitter<number> = new EventEmitter<number>();

  constructor() {/* empty */ }

  ngOnInit(): void {
    /* empty */
  }
  public handleClick(): void {
    this.navigate.emit(this.posterMovie.id);
  }
}
