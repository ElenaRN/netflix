import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { MoviesRoutingModule } from './movies-routing.module';
import { MoviesComponent } from './movies.component';
import { MoviesListComponent } from './movies-list/movies-list.component';

import { MoviesListService } from './movies-list/services/movies-list.service';
import { MoviesDetailsService } from './movies-details/services/movies-details.service';
import { MoviesVideosService } from './movies-videos/services/movies-videos.service';
import { MovieListItemComponent } from './movies-list/movie-list-item/movie-list-item.component';
import { MoviesDetailsComponent } from './movies-details/movies-details.component';
import { MoviesVideosComponent } from './movies-videos/movies-videos.component';



@NgModule({
  declarations: [
    MoviesComponent,
    MoviesListComponent,
    MovieListItemComponent,
    MoviesDetailsComponent,
    MoviesVideosComponent
    ],

  imports: [
    CommonModule,
    MoviesRoutingModule,
    RouterModule,
  ],
  providers: [MoviesListService, MoviesDetailsService, MoviesVideosService],
})
export class MoviesModule { }

