import { TestBed } from '@angular/core/testing';

import { MoviesVideosService } from './movies-videos.service';

describe('MoviesVideosService', () => {
  let service: MoviesVideosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MoviesVideosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
