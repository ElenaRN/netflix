import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { ApiResponseVideos, ApiResponse, Videos } from '../models/movies-videos-model';


@Injectable({
  providedIn: 'root'
})
export class MoviesVideosService {

  private baseUrl = ' https://api.themoviedb.org/3/';
  private queryMovies = 'movie/';
  private queryVideos = '/videos?';
  private key = 'api_key=f8da09545bcfbe3ebb9b87d6d72e5c19';
  private queryLanguage = '&language=es';

  constructor(private http: HttpClient) { /* empty */ }
  public getMoviesVideos(movieId: string): Observable<Videos[]> {

    const moviesVideosObs: Observable<Videos[]> =

      this.http.get(this.baseUrl + this.queryMovies + movieId + this.queryVideos + this.key + this.queryLanguage)
        .pipe(

          map((res: ApiResponseVideos) => {
            console.log(res);
            if (!res) {
              throw new Error('Not found videos');
            } else {
              //  return res;

              const results: ApiResponse[] = res.results;

              const formattedResultsVideos = results.map(({ id, key, name }) => ({
                id,
                key,
                name
              }));
              return formattedResultsVideos;
            }
          }),
          catchError(err => {
            throw new Error(err.message);
          })
        );

    return moviesVideosObs;

  }
}

