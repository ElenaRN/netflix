import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviesVideosComponent } from './movies-videos.component';

describe('MoviesVideosComponent', () => {
  let component: MoviesVideosComponent;
  let fixture: ComponentFixture<MoviesVideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoviesVideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesVideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
