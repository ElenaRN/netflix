import { ApiResponse, Search } from './../models/navbar-search';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {
  private baseUrl = 'https://api.themoviedb.org/3/';
  private paramMovies = 'movie/';
  private key = 'f8da09545bcfbe3ebb9b87d6d72e5c19&language=es';
 

  constructor(private http: HttpClient) { }

  public getSearchForm(search: Search, movieId: string): Observable<Search[]> {

    /* const queryParams = '/characters?key='
    + this.key + '&name=' + search.name + '&house=' + search.house + '&bloodStatus=' + search.bloodStatus; */
    let queryParams = this.paramMovies + movieId + '?api_key=' + this.key ;
    if (search.title){
      queryParams += '&title=' + search.title;
    }

    const searchsObs: Observable<Search[]> =
      this.http.get(this.baseUrl + queryParams)
        .pipe(
          map((searchRes: ApiResponse[]) => {

            console.log(searchRes);
            if (!searchRes) {
              throw new Error('error de resultado!');
            } else {
              const formattedSearch: Search[] = searchRes.map(({ title }) => ({
               title,
              }));

              return formattedSearch;
            }
          }),
          catchError(err => {
            throw new Error(err.message);
          })
        );
    return searchsObs;
  }
}
