import { Search } from './models/navbar-search';
import { NavbarService } from './services/navbar.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { faSearch, faBars } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  faSearch = faSearch;
  faBars = faBars;

  public searchFormGroup: FormGroup = null;
  public submitted = false;
  public searchList: Search[];
  public movieId: string;

  constructor(private formBuilder: FormBuilder, private navbarService: NavbarService, private route: ActivatedRoute) {

    this.searchFormGroup = this.formBuilder.group({
      title: ['', [Validators.minLength(5)]],

    });
  }

  ngOnInit(): void {

  }
  onSearchSubmit(): void {
    this.submitted = true;

    if (this.searchFormGroup.valid) {
      // Creamos un artículo y lo emitimos
      const search: Search = {
        title: this.searchFormGroup.get('title').value,

      };
      console.log(search);
      this.getSearchList(search);
      this.submitted = false;
    } else {
      console.log('Contiene errores :(');
    }
  }
  public reset(): void {
    this.searchFormGroup.reset();
  }
  private getSearchList(search: Search): void {
    this.route.paramMap.subscribe(params => {
      this.movieId = params.get('id');
    });

    this.navbarService.getSearchForm(search, this.movieId).subscribe((data: Search[]) =>{
      this.searchList = data;
      console.log(data);
    },
    (err: any) => {
      console.error(err);
    });
  }
}
